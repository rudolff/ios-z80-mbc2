
// -------------------------------------------------
//         Opcode     Exchanged bytes  Description
// -------------------------------------------------
enum {
  // Write Operations
  OP_USER_LED   = 0,    // 1
  OP_SERIAL_TX  = 1,    // 1
  OP_GPIOA_WR   = 3,    // 1
  OP_GPIOB_WR   = 4,    // 1
  OP_IODIRA_WR  = 5,    // 1
  OP_IODIRB_WR  = 6,    // 1
  OP_GPPUA_WR   = 7,    // 1
  OP_GPPUB_WR   = 8,    // 1
  OP_SELDISK    = 0x09, // 1
  OP_SELTRACK   = 0x0A, // 2
  OP_SELSECT    = 0x0B, // 1  
  OP_WRITESECT  = 0x0C, // 512
  OP_SETBANK    = 0x0D, // 1
  OP_SETTMRFREQ = 0x10, // 2  Set timer frequency  bytes, 1 to 65535 Hz
  OP_ENTMRINT   = 0x11, // 1  Enable timer interrupt
  OP_TMRINTCODE = 0x12, // 1  Code (instruction in IM0 or low address byte in IM2) on data bus when interrupt happened

      // Read Operations:
  OP_USER_KEY   = 0x80, // 1
  OP_GPIOA_RD   = 0x81, // 1
  OP_GPIOB_RD   = 0x82, // 1
  OP_SYSFLAGS   = 0x83, // 1
  OP_DATETIME   = 0x84, // 7
  OP_ERRDISK    = 0x85, // 1
  OP_READSECT   = 0x86, // 512
  OP_SDMOUNT    = 0x87, // 1
  OP_READTMR    = 0x88, // 2  Read timer value
  OP_NOP        = 0xFF  // 1  No operation  
  };
