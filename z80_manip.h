#ifndef _Z80_MANIP_DEFINED
#define _Z80_MANIP_DEFINED

#include <avr/io.h>
#include <Arduino.h>
#include "hardware.h"

// Z80 bootstrap routines

const byte    LD_HL        =  0x36;       // Opcode of the Z80 instruction: LD(HL), n
const byte    INC_HL       =  0x23;       // Opcode of the Z80 instruction: INC HL
const byte    LD_HLnn      =  0x21;       // Opcode of the Z80 instruction: LD HL, nn
const byte    JP_nn        =  0xC3;       // Opcode of the Z80 instruction: JP nn
const byte    LD_A_HL      =  0x7E;


void pulseClock(byte numPulse); // Generate <numPulse> clock pulses on the Z80 clock pin.
void loadByteToRAM(byte value); // Load a given byte to RAM using a sequence of two Z80 instructions forced on the data bus.
byte getByteFromRAM();
void loadHL(word value); // Load "value" word into the HL registers inside the Z80 CPU, using the "LD HL,nn" instruction.
void incHL(); // Execute the INC(HL) instruction (T = 6). See the Z80 datasheet and manual.
void singlePulsesResetZ80(); // Reset the Z80 CPU using single pulses clock

#endif // _Z80_MANIP_DEFINED
