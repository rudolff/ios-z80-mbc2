#ifndef _HARDWARE_DEFINED
#define _HARDWARE_DEFINED


// MACROS to replace runtime mapping of port pins
#define DIRECT_ACCESS 1
#if DIRECT_ACCESS

#define SETBIT(PORT, BIT) (PORT |= (1<<(BIT)))
#define CLRBIT(PORT, BIT) (PORT &= ~(1<<(BIT)))
#define _WRITE(PORT, BIT, LEVEL) ((LEVEL) != LOW ? SETBIT(PORT, BIT) : CLRBIT(PORT, BIT))

#define digitalWrite(PIN, LEVEL) (PIN>=32 ? 0 : \
      (PIN>=24 ? _WRITE(PORTA, (PIN)-24, LEVEL) : \
      (PIN>=16 ? _WRITE(PORTC, (PIN)-16, LEVEL) : \
      (PIN>=8 ? _WRITE(PORTD, (PIN)-8, LEVEL) : _WRITE(PORTB, PIN, LEVEL)))))

#define GETBIT(PORT, BIT) (PORT & (1<<(BIT)) ? HIGH : LOW)
#define digitalRead(PIN) (PIN>=32 ? 0 : \
      (PIN>=24 ? GETBIT(PINA, (PIN)-24) : \
      (PIN>=16 ? GETBIT(PINC, (PIN)-16) : \
      (PIN>=8 ? GETBIT(PIND, (PIN)-8) : GETBIT(PINB, PIN)))))      

#endif // DIRECT_ACCESS

// ------------------------------------------------------------------------------
//
// Hardware definitions for A040618 (Z80-MBC2) - Base system
//
// ------------------------------------------------------------------------------

// Z80 data bus
#define   D0        PIN_PA0
#define   D1        PIN_PA1
#define   D2        PIN_PA2
#define   D3        PIN_PA3
#define   D4        PIN_PA4
#define   D5        PIN_PA5
#define   D6        PIN_PA6
#define   D7        PIN_PA7

#define   AD0       PIN_PC2   // Z80 A0
#define   WR_       PIN_PC3   // Z80 WR
#define   RD_       PIN_PC4   // Z80 RD
#define   MREQ_     PIN_PC5   // Z80 MREQ
#define   RESET_    PIN_PC6   // Z80 RESET
#define   MCU_RTS_  PIN_PC7   // * RESERVED - NOT USED *
#define   MCU_CTS_  PIN_PD2   // * RESERVED - NOT USED *
#define   BANK1     PIN_PD3   // RAM Memory bank address (High)
#define   BANK0     PIN_PD4   // RAM Memory bank address (Low)
#define   INT_      PIN_PB1   // Z80 control bus
#define   RAM_CE2   PIN_PB2   // RAM Chip Enable (CE2). Active HIGH. Used only during boot

#define   SS_       PIN_PB4   // SD SPI
#define   MOSI      PIN_PB5   // SD SPI
#define   MISO      PIN_PB6   // SD SPI
#define   SCK       PIN_PB7   // SD SPI
#define   BUSREQ_   PIN_PD6   // Z80 BUSRQ
#define   CLK       PIN_PD7   // Z80 CLK
#define   SCL_PC0   PIN_PC0   // IOEXP connector (I2C)
#define   SDA_PC1   PIN_PC1   // IOEXP connector (I2C)
#define   LED_IOS   PIN_PB0   // Led LED_IOS is ON if HIGH
#define   WAIT_RES_ PIN_PB0   // Reset the Wait FF
#define   IORQ_     PIN_PD2   // Z80 IORQ, MCU INT0
#define   USER      PIN_PD5   // Led USER and key (led USER is ON if LOW)


// ------------------------------------------------------------------------------
//
// Hardware definitions for A040618 GPE Option (Optional GPIO Expander)
//
// ------------------------------------------------------------------------------

#define   GPIOEXP_ADDR  0x20  // I2C module address (see datasheet)
#define   IODIRA_REG    0x00  // MCP23017 internal register IODIRA  (see datasheet)
#define   IODIRB_REG    0x01  // MCP23017 internal register IODIRB  (see datasheet)
#define   GPPUA_REG     0x0C  // MCP23017 internal register GPPUA  (see datasheet)
#define   GPPUB_REG     0x0D  // MCP23017 internal register GPPUB  (see datasheet)
#define   GPIOA_REG     0x12  // MCP23017 internal register GPIOA  (see datasheet)
#define   GPIOB_REG     0x13  // MCP23017 internal register GPIOB  (see datasheet)

// ------------------------------------------------------------------------------
//
// Hardware definitions for A040618 RTC Module Option (see DS3231 datasheet)
//
// ------------------------------------------------------------------------------

#define   DS3231_RTC    0x68  // DS3231 I2C address
#define   DS3231_SECRG  0x00  // DS3231 Seconds Register
#define   DS3231_STATRG 0x0F  // DS3231 Status Register

#endif // _HARDWARE_DEFINED
