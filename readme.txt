https://github.com/MCUdude/MightyCore#how-to-install

* How to install *

Boards Manager Installation
This installation method requires Arduino IDE version 1.6.4 or greater.

* Open the Arduino IDE.
* Open the File > Preferences menu item.
* Enter the following URL in Additional Boards Manager URLs:
  https://mcudude.github.io/MightyCore/package_MCUdude_MightyCore_index.json
  Separate the URLs using a comma ( , ) if you have more than one URL
* Open the Tools > Board > Boards Manager... menu item.
* Wait for the platform indexes to finish downloading.
* Scroll down until you see the MightyCore entry and click on it.
* Click Install.

After installation is complete close the Boards Manager window.


Fuse bits: High Byte 0xD6, Low Byte 0xAF, Lock Byte 0xCF